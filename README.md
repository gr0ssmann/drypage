# drypage

(pronounced: *Don't Repeat Yourself, Page*)

drypage allows you to impose a "general" behavior of oTree `Page`s while allowing for exceptions and removing the need for repetition. It acknowledges the superiority of the new no-self approach while addressing some advantages of subclassing and inheritance.

## The problem

[oTree](https://www.otree.org/) used to permit a powerful form of subclassing in `Page`s, for example:

```python
class SuperPage(Page):
    def is_displayed(self, player):
        return not player.participant.vars['dropout']

class MyPage(SuperPage):
    pass

class MyPage2(SuperPage):
    pass
```

With the removal of the `self` arguments, this would still work. And this here, too, but only in very simple circumstances:

```python
def is_not_dropout(player):
    return not player.participant.vars['dropout']

class MyPage(Page):
    is_displayed = is_not_dropout

class MyPage2(Page):
    is_displayed = is_not_dropout
```

Already we see some repetition.

However, what if the behavior of `is_displayed` should *occasionally* depend on the specific page? With the self format, that was no worry: `self` could be used in all ways imaginable by the `SuperPage` to use attributes of the actual page. Typically, this will now require repetition as follows:

```python
def is_dropout(player):
    return player.participant.vars['dropout']

class MyPage(Page):
    @staticmethod
    def is_displayed(player):
        return not is_dropout(player) and not player.skip

class MyPage2(Page):
    @staticmethod
    def is_displayed(player):
        return not is_dropout(player)
```

This issue arises because `is_displayed` and `is_dropout` nowadays are *not aware of the class/page they belong to* or that is currently running the code. The lack of `self` means that the methods are "floating in cyberspace". With two pages, it might not be a big deal, but if you have many pages, this is a major pain -- especially if you need to make changes later on or debug. Needless to say, there are various approaches you could take, but they may be less intuitive or cumbersome.

It is often desirable to specify a "general", "common" page behavior, but allowing for *some* variation. With drypage, you can now do:

```python
import drypage

...

class SuperPage(Page): # the superpage must derive from Page
    @drypage.extend # denotes that this method should be "transplanted"
    def is_displayed(player, page):
        # this here is new:  ^^^^
        
        condition = True # the default if the page does not customize
        
        try:
            # if the page customizes the display behavior, then use that
            
            condition = page.display_hook(player)
        except AttributeError:
            pass
        
        return condition and not player.participant.vars['dropout']

@drypage.apply(SuperPage)
class MyPage:
    def display_hook(player):
        return not player.skip

@drypage.apply(SuperPage)
class MyPage2:
    pass
```

As you see, `SuperPage` is back, and its `is_displayed` has an extra argument: `page`. This allows the superpage (the `SuperPage`) to learn which page is actually calling the method. (We call `SuperPage` a superpage because it is logically superior to the true pages; it governs their behavior while never being shown to the subject. It is a pure abstraction.) In the example above, drypage transplants `SuperPage.is_displayed` onto `MyPage` and `MyPage2`, allowing these pages to share behavior while allowing customization by defining a `display_hook` method inside *them*. This avoid repetition. `MyPage2` should be interpreted as a "common" page which should not require much code. Clearly, this programming style allows the superpage to "refer back" to the (inferior) true page and to delegate some behavior or read some property, etc.

Furthermore, drypage raises exceptions for common pitfalls. In summary, drypage allows you to get back some old-school functionality without repeating yourself while having a lot of readability and being very explicit. Pretty neat, huh?

## How to use

1. Clone this repository
2. Move the resulting `drypage/` directory into your oTree **project**, i.e. `drypage/` should be on the same level as your `settings.py`.
3. Import `drypage` in your app's `__init__.py` and get started.

## Important

- The superpage must derive from `Page`, but the actual pages must not. drypage checks this.
- The additional argument passed to extended methods is a keyword argument named `page`. It can be at any position in the function signature, but it must have that exact name.
- You can extend any method with `@drypage.extend`, e.g. `vars_for_template`, `get_timeout_seconds`, ...
- Remember: You write *general* code in the superpage and *specific* code in the actual page. The *specific* code is then somehow invoked by the *general* code, depending on your application.
- The classes created by the decorator `@drypage.apply(SuperPage)` derive from the page and the superpage, **in that order**. drypage yields an exception if the page contains a method that would be overwritten because a method named such in the superpage class is marked with `@drypage.extend`, e.g. here (last example continued):

```python
@drypage.apply(SuperPage)
class MyPage3:
    def is_displayed(player):
        return True
```

This would be unsafe (creates confusion) and is not allowed. You would have to do this:

```python
class SuperPage(Page):
    @drypage.extend
    def is_displayed(player, page):
        try:            
            return page.display_override(player)
        except AttributeError:
            pass
        
        ...

...

@drypage.apply(SuperPage)
class MyPage3:
    def display_override(player):
        return True
```

Of course, this only makes sense if `SuperPage` contains some other method that *should* apply to `MyPage3`. If not, just avoid using `SuperPage`:

```python
class MyPage3(Page):
    pass
```
